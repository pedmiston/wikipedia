# Concerns

- It's easier for me to think about experiments than theory/review topics.
- I'm unsure of the best way to set up a successful dissertation.

What are the next steps in the study of human cultural evolution beyond demonstrating that Wikipedia is an evolutionary system? There are projects that I can think of that run with the idea that Wikipedia is a model organism for the study of cultural evolution.

- wikilinks: How does reader traffic flow affect the editing process?
- thumbs/stack_overflow: Do different feedback mechanisms (e.g., thumbs versus edits) result in more or less efficient selection pressures?

But I

## Open source software

Rather than narrowing in, another direction to go would be to extend the analysis of Wikipedia as an evolutionary system to a different domain, open source software. The analysis conducted for the evaluation of Wikipedia as an evolutionary system can also be conducted on open source software projects. The experiment would comprise training a machine learning algorithm to detect whether or not a particular commit was helpful or not.

There are a number of technical problems that would need to be overcome. For instance, it's unclear whether there is a version controlled record of rejected pull requests. Obtaining the data to train the machine learning algorithm would also be tricky and require some level of expertise.

**A potential topic for the depth prelim** would be to review the history of open source software, in particular the motivations behind distributed development and free software licensing. There is a reason


# Tier 2

I want to argue that cognition accumulates.
