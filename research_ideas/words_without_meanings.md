![An illustration of Lewis Carroll's nonsense poem "Jabberwocky". Otherwise meaningless words come to life when given a little context.](words_without_meanings/jabberwocky.jpg "An illustraction of Lewis Carroll's Jabberwocky")

# Words without meanings

The popularity of Chomsky’s nonsense sentence “colorless green ideas sleep furiously” seems to contradict the very purpose he had in writing it. The sentence, which is syntactically valid but illogical, was intended to capture the view that language was defined by rules, not by meaning. Today the sentence stands as a canonical example of Chomksy's approach to human language, imbuing the meaningless sentence with enough meaning to warrant it’s own [Wikipedia page](https://en.wikipedia.org/wiki/Colorless_green_ideas_sleep_furiously).

Chomsky's sentence is only syntactically valid because it is made of words English speakers already know. But even words we've never heard before can be readable, as evidenced by Lewis Carroll's poem "Jabberwocky". Each line of the poem looks and sounds just a bit like something I've heard before, enough to make the phrases have meaning even if the individual words are as mysterious as story in the poem.

> 'Twas brillig, and the slithy toves
> Did gyre and gimble in the wabe;
> All mimsy were the borogoves,
> And the mome raths outgrabe.

It's easy to attempt the meaning of individual words, and it turns out that these guesses are not as random as you might think. People who speak the same language are likely to guess similarly because the novel word might look or sound like words that both of them know. Even people who speak different languages agree about the guessed meaning of some words. That these guesses are not random demonstrates that even supposedly meaningless words have some context for interpretation.

![Which shape is the bouba and which shape is the kiki? Across languages people seem to agree that some sounds are more likely to be associated with some visual features when given a choice.](words_without_meanings/bouba_kiki.png "The bouba kiki effect")

Certainly there are perfectly meaningless words. If I open a copy of _Madame Bovary_ in it's native French, I'm not going to get much of the plot. But if I see it acted out in French, I'll know pretty much what is happening even if I don't understand the individual words. Is spoken French meaningless? If you've traveled in a foreign country you know that languages you don't speak are far from meaningless. But the meaning is constrained by the interaction, the common ground between the speaker and the listener.

Are there spoken meaningless words? Words without meaning is a privilege of written language, not human language as a whole. A made up word, if detached of a meaning simply will not survive in the lexicon unless it is written down. The first dictionaries were astoundingly difficult and the lexographers felt it was their duty to preserve words whose meanings we may some day forgot.

Words without meaning is a privilege of the literate world.

The ability to parse words from a speech stream is a landmark for infants beginning to learn a language. Learning to speak is a dynamic process as the words you know continually changes the words you are exposed to. Early on, words are far from symbolic, and the complicated process by which kids become able to generalize a new word to a new context is one of the key  questions in the cognitive psychology of language development.

For adults, however, once a word is learned, it is integrated with everything else that we know. If I tell you that “whizbat” is the name of a new game I’ve been playing, you might infer that in whizbat there are winners and losers.

As experienced language users we know we can reason with words. Presumably the inference that whizbat has winners and losers was something we reasoned about. All games have winners and losers, and whizbat is a sport, therefore whizbat must have winners and losers. Of course not all games have winners and losers, and human language has never been great at obeying the laws of logic, so we could relax our inference to some games have winners and losers, and whizbat is a game, therefore it is likely that whizbat has winners and losers. But the idea that this is rational behavior still stands.
