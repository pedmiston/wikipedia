# Text Boxes

Participants contributed to ongoing, collaborative writing projects under various constraints. Constraints were implemented on the types of edits that were accepted. In the first experiment we varied the ratio of additional words to the removal of old words that was required. At one extreme, only the addition of words is allowed. At the other end, only as many words as were removed can be added.
