---
title: |
    Thumbs up or thumbs down? Selection pressures in the cultural
    evolution of recommendation system micro-cultures
author: Pierce Edmiston
---

When people are interacting with a recommendation system, which type of
feedback leads to the greatest convergence of answers?

This is a marketing question, but scientifically it is a question
concerning the selection pressures in the micro-society that is
iterative recommendation systems.

# Design

The intention of the experiment is not to identify an overall winner
between thumbs up and thumbs down recommendation systems. Intuitively it
seems that recommendation systems would have the potential to benefit
the most from the most types of data, in which case we would expect 
data from thumbs in both directions to lead to the most convergence
among respondents. This intuition is tested in the present experiment by
comparing the outcomes of three different recommendation systems: one
that only allows its users to "like" something, one that only allows
"dislikes", and a third group that is allowed to either up or down vote.

The predictions are straightforward. The system with both up and down
votes should result in the greatest convergence among respondents,
because it increases the dimensionality of the iterative process.
Without the ability to explore possible outcomes in multiple dimensions,
the group may too easily fall into a local minimum.

The prediction for the valence comparison between the up-voters and the
down-voters is less clear. Is it better to voice your support of a
particular cultural variant, or is it more efficient to rule out the
ones everyone can agree are bad?

It is important to consider the task when predicting the outcomes of the
various groups. The measure of success in this experiment, but not in
real life, is convergence. Participants are rating something with the
knowledge that their goal is for everyone to agree on something. Whether
or not convergence is predictive of future satisfaction or success is
left as an exercise to the reader. What is measured in this experiment
is whether or not the decision of the group improves over generations.

# Experiment 2: How much is too much?

Intuitively it seems that the combination of binomial and continuous
ratings systems to be the best.  Netflix is just one company that
invests heavily in guessing what it's consumers will enjoy, and they no
doubt measure more than just star ratings to recommend new content to
its users. However, it also seems likely that at some point the addition
of a new measure no longer leads to the same improvement in
recommendation quality. It's unclear that if Netflix asked its users to
fill out extensive surveys after every interaction that it's quality as
a recommendation system would be improved dramatically[^1].

[^1]: For starters, too many surveys would damage the user experience,
and Netflix might lose customers who became frustrated by the invasive
invitations to provide verbose feedback. Even if the surveys were optional, the
quality of the feedback that is provided may be compromised. The
solution employed by most internet companies today is to measure all
user behavior (hence, big data is born), and to ask feedback of a subset
of users. Whether or not this strategy is successful is apparent to
anyone familiar with the coining of the term "clickbait".


