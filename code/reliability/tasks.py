import sys

from invoke import task
from bs4 import BeautifulSoup
import requests
import pandas


@task
def get(output=None):
    """Get the page refs in a csv."""
    url = 'https://en.wikipedia.org/wiki/Reliability_of_Wikipedia'
    response = requests.get(url)
    soup = BeautifulSoup(response.content.decode('utf-8'), 'html.parser')
    refs = [clean(x.get_text()) for x in soup.select('ol.references > li')]
    data = pandas.DataFrame({'bib': refs})
    handle = open(output, 'w') if output else sys.stdout
    data.to_csv(handle, index=False)
    handle.close()

def clean(x):
    return x.strip('[^\ \xa0]')
