from invoke import task, run
import requests
from unipath import Path


project_root = Path('~/Research/Wikipedia').expand_user()
grant_dir = Path(project_root, 'documents/grant')


@task
def get():
    """Get the current version of the proposal wiki."""
    root_url = 'https://meta.wikimedia.org/w/api.php'

    # Add page title as a string so the special characters aren't changed
    root_url += '?titles=Grants:IEG/Learning_from_article_revision_histories'

    kwargs = dict(
        action="query",
        prop="revisions",
        rvprop="content",
        format="json",
    )
    r = requests.get(root_url, kwargs)

    data = r.json()
    pages = data['query']['pages']
    page_ids = list(pages.keys())
    assert len(page_ids) == 1, "more than one page was retrieved"

    revisions = pages[page_ids[0]]['revisions']
    assert len(revisions) == 1, "more than one revision was retrieved"

    current = revisions[0]['*']
    current = current.encode('utf-8')

    dst = Path(grant_dir, 'ieg.wiki')
    with open(dst, 'w') as out:
        out.write(current)


@task
def cp():
    """Copies the IEG MediaWiki page to the clipboard"""
    cmd = 'cd {grant_dir} && cat ieg.wiki | pbcopy'
    run(cmd.format(grant_dir=grant_dir))


@task
def figs():
    """Compile figs used in the grant application."""
    figs = Path(grant_dir, 'figs').listdir('*.gv')
    formats = ['svg', 'pdf', 'png']
    cmd = 'dot -T{} -O {}'
    for fig in figs:
        for fmt in formats:
            run(cmd.format(fmt, fig))
