#!/usr/bin/env python
from invoke import task, run, Collection
import yaml
from unipath import Path

import ieg


project_root = Path('~/Research/Wikipedia').expand_user()
custom = yaml.load(open(Path(project_root, 'tasks/config.yml')))

@task
def build(doc):
    """Build documents using commands in config.yml."""
    if doc not in custom:
        config = dict(dir='docs/{}'.format(doc), cmd='./build')
    else:
        config = custom[doc]
    cmd = 'cd {project_root} && cd {dir} && {cmd}'
    run(cmd.format(project_root=project_root, **config))


namespace = Collection(build, ieg)
