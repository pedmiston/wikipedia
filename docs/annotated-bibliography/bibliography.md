---
title: Annotated bibliography for Wikipedia prelim
author: Pierce Edmiston
date: 1/9/2016
csl: apa-annotated-bibliography.csl
---

# Evolutionary theory

Although the following books are not primary sources for the prelim, they have
shaped my understanding of evolutionary theory and are worth mentioning here.

- @dawkins1976selfish
- @dennett1995darwin
- @blackmore2000meme
- @richerson2008not
- @mesoudi2011cultural

# Models of cultural evolution

Models of cultural evolution make simplifying assumptions about how information
spreads in a population (e.g., do people learn from their parents or from
their peers?) and the output of the model can be compared to real-world
scenarios. This sort of modeling work began with seminal books by
@cavalli1981cultural and @boyd1988culture.

Models of cultural evolution have been invoked in two domains: modeling the
distribution of cultural variants (the neutral model) and modeling the process
of historical change (phylogenetic analyses).

## The neutral model

The neutral model for cultural evolution describes a situation where there is variation and inheritance without selection pressures that deem some variants better than others. In population genetics, this is called random drift. For cultural evolution, deviations from the neutral model are crucial in order to say that selection has occurred. Otherwise, the distribution of variants might be the result of a random process of inheritance. The following papers outline situations where the neutral model does and doesn't apply to cultural evolution.

- @Bentley:2004dna
- @Rogers:2008bk
- @Lynch:1993kb
- @Mesoudi:2008jk

## Phylogenetic analysis of cultural products

In the following papers, the authors applied phylogenetic analyses to cultural products in order to determine the historical relationships between extant variants. It's somewhat surprising that the same basic statistical technique succeeds in such diverse domains, and it is illustrative of the power of evolutionary thinking. The accuracy of any phylogenetic technique depends on modeling in the sense that the model must account for how variation accumulates incrementally.

- Language family trees
  + @Pagel:2009haa
- Transcription of manuscripts
  + @Barbrook:1998wh
  + @Howe:2001ty
  + @Spencer:2004ci
- Archaeological technologies (arrowheads)
  + @Bettinger:1999vv
  + @OBrien:2001ub
- Other cultural products
  + textile patterns: @Tehrani:2002tx

# Experimental evolution

Although the modeling work is valuable, it must be balanced by experimental work
done in the lab. The same is true in biological evolution. @Elena:2003fr review
the key findings drawn from evolution experiments conducted on bacteria and
other quickly-reproducing species in the lab. For experiments on cultural
evolution, Whiten and Mesoudi [-@Whiten:2016fp] provide a thorough overview of
research on diffusion of information in humans and animals. A few key studies
are recorded here.

- @Barrett:2001us
- @Schotter:2003hz
- @Kirby:2008ui
- @Mesoudi:2008vi and @Mesoudi:2008we
- @Caldwell:2009fl
- @Mesoudi:2010jn
- @Caldwell:2010bq
- @Caldwell:2014eb

# Wikipedia

The following articles are specific to Wikipedia. Although they are published
in a variety of places (conference proceedings and open source journals),
they serve as a jumping off point for further research on Wikipedia.

- @Wilkinson:2007wt
- @Gorgeon:2011ia
- @Callahan:2011km
- @Anonymous:fOSBiMWv
- @Bellomi2005:na

# The Baldwin Effect

In evolutionary biology, the Baldwin Effect [@Baldwin:1896du] represents the
method by which characteristics that are acquired in one's lifetime can be
passed on to subsequent generations in a way that affects their reproductive
fitness without requiring a modification of the genome ("Lamarckism"). Baldwin
argued that behaviors that were learnable could be shared across generations,
significantly altering the path of genetic evolution by changing the fitness
landscape of the individual. Nearly one hundred years later, @Hinton:1987uw
modeled the basic principles of the Baldwin effect in a neural network,
demonstrating that an organism's fitness can benefit from learning even if those
changes aren't reflected in the genome (network architecture) of the individual.

I believe that understanding the Baldwin Effect may be critical to understanding
how Wikipedia is able to continually improve in quality. A typical explanation
given for why Wikipedia articles are high quality appeals to the so-called
"wisdom of the crowd" [@surowiecki2005wisdom; @Arazy:2006tp]. However, there are
a number of reasons such an explanation is inadequate for explaining the success
of Wikipedia, namely that in a typical wisdom of the crowd experiment, each
guesser has some probability of coming up with the correct answer, which isn't
true in the case of Wikipedia.

When viewed as an evolutionary system, there is a different possible explanation
for Wikipedia's success in terms of the Baldwin Effect. Rather than hundreds of
edits occurring independently of one another, edits are cumulative, and someone
who edits the article later on can learn from the previous edits, allowing the
article to increase in quality. Wikipedia is a special case of iterative wisdom
of the crowds, where each new edit is not independent of the previous edits. The
Baldwin Effect as applied to Wikipedia may be crucial to unlocking any
meaningful sense of an "optimal edit", and it is a subject that is ripe for
modeling.

# Annotated bibliography
