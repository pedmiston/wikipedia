
- How successful is the Wikipedia editing strategy in only allowing articles to improve in quality through incremental edits and reversions?
- What is that allows for humans to engage in productive open collaboration?
- What is the role of open collaboration in understanding human cultural evolution?
- To what extent is the success of Wikipedia attributable to the effects of cultural learning as opposed to the contributions of individual editors working independent of one another?
