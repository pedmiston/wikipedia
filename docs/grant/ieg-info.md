# Individual Engagement Grants

## Eligible projects

- 6 month scope, with potential renewal
- Maximum request is $30,000

> Projects should foster conditions that encourage editing by
  volunteers.

## Selection criteria

- Impact potential
- Innovation and learning
- Ability to execute
- Community engagement
- The total amount of funding available

## Timeline

- Proposals accepted 1-31 March
- Committee review: 1-15 April
- Grantees announted: 29 April
- Final reports: November


