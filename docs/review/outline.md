---
title: |
    Wikipedia as a model organism for the study of cultural evolution
author: Pierce Edmiston
---

# Introduction

The challenge to a scientific understanding of cultural change is determining where cultural change is not random. Wikipedia is proposed as a test case where change should not be random. The idea of evolution as an algorithm is described.

# The tree of life and the tree of culture

Contrary to intuitions about the messiness of cultural inheritance, ancestral trees of some cultural products can be reconstructed. However, in neither genetic evolution nor cultural evolution do ancestral trees explain why some change happened. More important than being able to retrace the steps of history is explaining which steps occurred for a reason.

- Language family trees
  + @Pagel:2009haa
- Transcription of manuscripts
  + @Barbrook:1998wh
  + @Howe:2001ty
  + @Spencer:2004ci
- Archaeological technologies (arrowheads)
  + @Bettinger:1999vv
  + @OBrien:2001ub
- Other cultural products
  + textile patterns: @Tehrani:2002tx

# Evidence of selection

The strongest evidence of selection is when independent 

Distributions that appear random may not be, and those that appear
non-random may indeed be unpredictable.

For evolutionary systems, the null hypothesis is that the survival of
certain variants is random.

- The logic and implications of the neutral model
  + (find an article about genetic drift)
  + @Bentley:2004dna
  + @Lynch:1993kb
- Cases where the neutral model does not describe human culture
  + @Rogers:2008bk
  + @Mesoudi:2008jk

When it comes to Wikipedia, the null hypothesis takes a number of forms.

#. Edits are just as likely to make an article better as worse.
  + If the quality of an article can be quantified, then the changes
    to article quality over time would fail the Page rank test
    [@Page:1963hl].
#. The distribution of articles that improve compared to those that
   don’t improve is neutral.

In other cases we might ponder why something is non-random. It's key to
think about convergent evolution in these cases. Why do all writing
systems seem to settle on about 3 strokes per character and a
stroke-type to character-set redundancy of about 50% [@Changizi:2005cm]?

The authors try to conclude that the parameters that were converged upon
may reflect the constraints of the visual system, but really the only
way to know is to run experiments.

# Fitness landscapes

Convergence and reliable improvements are two indicators of non-randomness in evolutionary systems.

- convergent evolution
- Lenski experiments



There is no universal unit of success in biology or in culture,
so conclusions about whether or not a system is evolving depend
almost entirely on the units.

- Cumulative cultural evolution:
  + @Caldwell:2008
  + @Caldwell:2009fl
  + @Caldwell:2010bq

## Success is not popularity

> Why should we care about iteration instead of ability?


## Cognitive inheritance

Most of what we learn, we learn from other people, and the question of
whether a population believes something does not always reduce to what
an individual tends to believe.

The problem with the meme theory of cultural evolution is that it is limited to cases in which individuals must perfectly reproduce the meme. It must exist in each individual in the lineage. Yet there are many cultural products for which this is definitely not the case, Wikipedia articles being a clear example.

The entirety of a Wikipedia article does not live in someone's brain, just waiting to be written down.

- Examples of situations in which individual learning does not scale:
  + @Asch found that individuals conform, but @Jacobs:1961 found that
    populations do not.
  + @Horner:2005 found that children imitate even irrelevant actions,
    but populations of children do not.
- Experimental evolution on implicit change:
  + @Kirby:2008ui




# Wikipedia

> Is the evolution of Wikipedia articles cumultative or emergent?

If Wikipedia is just about getting smart people to write down what they know,
it is less interesting as an evolutionary system because then it would be a
case where there is explicit design invoked on the system.

For biological evolution, Darwin’s insight was that with natural selection, you can have design without a designer: complexity without any entity knowing what the end result will be.

- Baldwin effect
- wisdom of the crowds

Predictions for sites other than Wikipedia.

- Quora [@Maity:2015ux]

# Conclusions

- evolution implies non-randomness
- the test for experimental evolution is incremental improvement
- valuable improvement in cultural evolution is a psychology question
- Wikipedia is a good test case for many of these questions
