---
title: Adaptation in the evolution of cultural products
author: Pierce Edmiston
---

# Thesis statement

A general theory of evolution characterizes historical changes in species' DNA as well as cultural change in the history of human civilizations. Genetic and cultural evolution have many differences, but what they have in common--and the reason to label both systems "evolutionary"--is that they are adaptive. Adaptive systems are guaranteed to yield improved solutions to specific problems. Adaptive systems are powerful problem-solving techniques because they succeed even when the mechanism for generating new solutions is entirely random, as is the case in genetic evolution where variability is the result of a random process. What's not random is the selection pressures that evaluate all solutions in a generation and select only the best ones to survive. If selection pressures are not consistently applied, the system as a whole cannot be guaranteed to be adaptive, and much of genetic and cultural evolution is random in this way. But when selection pressures are consistently applied then solutions can be expected to only increase and never decrease in some measure of fitness. The tell-tale sign of an adaptive system is monotonic growth in some fitness function over generations under the critical condition that the environment is stable over this time.

![The properties of adaptive systems are embodied in many domains, including genetic evolution, cultural evolution, and evolutionary computing.][image-1]

# Biological evolution in a controlled environment

Discuss the Lenski long term evolution experiments in detail in arguing that the fundamental prediction to come from adaptive systems and Darwinian theory is that in a controlled environment, there should be a reliable monotonic increase in some measure of fitness.

# One evolution or many evolutions

What happens when genetic and cultural selection pressures are in competition? For example, when the same information can either be genetically determined or learned, who wins? Most of the time, genetic evolution wins because even if cultural and genes are passed only vertically, it is likely that genetic transmission is more reliable than cultural transmission. But there are situations in which cultural evolution prevails, namely, when a single individual can transmit information via oblique transmission to more people than offspring and when there exist culturally learnable solutions that have no representation in the genetic domain [@CavalliSforza:1983wx; @Thompson:2016ca].

[image-1]:	fig/evo_venn.png