---
title: Evolutionary systems
author: Pierce Edmiston
---

# Thesis

What are the core elements that all evolutionary systems have in common (Fig. 1)? I argue that a defining feature of evolutionary systems is the consistent application of selection pressures across generations of variants. Only when selection pressures are consistently applied can the iteration of the evolutionary process be expected to improve solutions in a particular domain.

![](fig/evo_venn.png)
