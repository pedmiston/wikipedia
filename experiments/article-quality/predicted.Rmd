---
title: "Predictors of article quality"
author: "Pierce Edmiston"
date: "`r Sys.Date()`"
output:
  html_document:
    theme: flatly
    toc: true
---

```{r, config, echo = FALSE}
library(knitr)
opts_chunk$set(
  echo = FALSE,
  message = FALSE,
  warning = FALSE,
  fig.width = 5,
  fig.height = 5,
  fig.path = "predicted-figs/"
)
```

The purpose of this experiment is to demonstrate that articles are selected for
quality. A lot hinges on how "quality" is defined, but the predictions are
the same: if the Wikipedia community selects for quality, then articles with
more edits should be of higher quality.

Although it may seem obvious that articles with more edits are of higher 
quality, there are a number of reasons this basic relationship may not hold. For
instance, an article on a particular math concept may start out small and 
readable for the non-expert, but once all obscure alternative formulas are 
thrown into the mix, the article may no longer be beneficial for someone new to 
the concepts. In addition, contentious articles may accumulate edits that go
back and forth without actually increasing the quality of the article.

```{r, setup}
library(dplyr)
library(ggplot2)

base_theme <- theme_minimal() +
  theme(
    axis.ticks = element_blank(),
    legend.position = "none"
  )

set.seed(153)
```

# Wikipedia ratings

Wikipedia has its own system for rating articles. The best rating a Wikipedia
article can get is **FA** for Featured Article. Only about ~0.1% of Wikipedia
articles have this rating.

![](img/rated-articles.png)

```{r, wiki-rating}
jitter <- function(dev = 5) rnorm(1000, sd = dev)

wiki_edits <- data_frame(
    sum_edits = 1:1000 + jitter(),
    quality = seq(1, 8, length.out = 1000) + jitter(1)
  ) %>% 
  filter(quality >= 1, quality <= 9, sum_edits > 0) %>%
  mutate(quality = round(quality, digits = 0))

scale_x_edits <- scale_x_continuous(
  "Total number of edits"
)

scale_y_quality <- scale_y_continuous(
  "Article quality",
  breaks = 1:9,
  labels = c("List", "Stub", "Start", "C", "B", "GA", "A", "FL", "FA")
)

ggplot(wiki_edits, aes(x = sum_edits, y = quality)) +
  geom_point(shape = 1, position = position_jitter(height = 0.2, width = 0.0)) +
  geom_smooth(method = "lm", se = FALSE, size = 1) +
  scale_x_edits +
  scale_y_quality +
  annotate("text", x = 40, y = 9, label = "simulated data", hjust = 0, fontface = "italic") +
  base_theme
```

# Subjective ratings

A different way to measure the quality of an article is simple to have people 
rate it directly. In this experiment, participants are given a Wikipedia article
and asked to rate its quality. They are given two versions of the same article.
On the view that articles get better over time, the later versions of the
article should be rated higher than earlier versions.

```{r, subjective-ratings}
quality <- data_frame(
    condition = rep(c("past_first", "current_first"), each = 2),
    order = rep(c(1, 2), times = 2),
    version = c(1, 2, 2, 1),
    quality = c(3.2, 5.1, 4.3, 2.4)
  ) %>% mutate(
    version = factor(version),
    condition_label = ifelse(condition == "past_first",
                             "Past version first", "Current version first")
  )


scale_x_version <- scale_x_discrete(
  "Article version",
  breaks = c(1, 2),
  labels = c("past", "current")
)

scale_x_order <- scale_x_continuous(
  "Rating order",
  breaks = c(1, 2),
  labels = c("first", "second")
)

ggplot(quality, aes(x = version, y = quality)) +
  geom_bar(aes(fill = version), stat = "summary", fun.y = "mean") +
  scale_x_version +
  base_theme

ggplot(quality, aes(x = order, y = quality)) +
  geom_bar(aes(fill = version), stat = "identity") +
  scale_x_order +
  facet_wrap("condition_label") +
  base_theme
```

# Other ways of measuring article quality

machine learning approaches

:   Machine learning approaches are beneficial in that they are scalable to the
    entire corpus of articles, and better account for the multidimensional 
    nature of article quality.

iterated imitation

:   Measuring how many generations a section of an article can be iterated 
    without losing fidelity is a unique metric for article quality. Although
    things that are memorable are not necessarily true, the purpose of 
    an encyclopedia article is to be accessible to the novice, and able to
    be related to others.
