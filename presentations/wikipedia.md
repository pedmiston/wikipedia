Selection pressures in Wikipedia
================================
author: Pierce Edmiston
date: 2015-12-18



Claims
======

- Wikipedia articles don't just get bigger, they also get **better**.
- Articles get better because Wikipedia is an **evolutionary system**.

<aside class="notes">
</aside>

What is an evolutionary system?
===============================

- An iterative process that yields better and better solutions to a problem (e.g., genetic reproduction).
- Evolution requires **variation** and **selection**.

<aside class="notes">
</aside>

Why biological evolution?
=========================

Biology originated the idea of an ever-changing fitness landscape.

![](wikipedia-figure/fitness-landscape.png)

<aside class="notes">
</aside>

Article evolution
=================

![](wikipedia-figure/castle.png)

Measuring article quality
=========================

Wikipedia's own ranking system.

![](wikipedia-figure/article-quality-chart.png)

A psychological definition of article quality
=============================================

_What can you do with a good article that you can't do with a bad one?_

- Learn more (remember more facts, afford better inferences).
- Find an answer to a question more quickly.
- Repeat it to someone else more accurately.

Edits
=====

Article histories are linear.

![plot of chunk linear](wikipedia-figure/linear-1.png) 

But in reality there are a lot of reversions.

![plot of chunk revisions](wikipedia-figure/revisions-1.png) 

Edit trees
==========

A better way to show article histories is as a tree.

![plot of chunk tree](wikipedia-figure/tree-1.png) 

This is how edits should be counted.

![plot of chunk edits](wikipedia-figure/edits-1.png) 

Optimal edits
=============

![](wikipedia-figure/article-quality-1.png)

Optimal edits
=============

![](wikipedia-figure/activity-session.png)

![](wikipedia-figure/total-time-1.png)

First steps
===========

- Learn the mathematics for understanding fitness landscapes.
- Write some scripts to parse Wikipedia article histories.
- Investigate relationships between number of edits and article quality,
  including number of unique editors, size of article, page views, etc.
- Run some simple experiments in which different versions of the same articles
  are compared (subjective judgments of quality, scavenger hunt, telephone game).
