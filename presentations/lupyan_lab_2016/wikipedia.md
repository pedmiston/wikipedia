Does Wikipedia evolve or is it assembled?
=========================================
author: Pierce Edmiston
date: 2016-01-28



Claims
======

- Wikipedia articles don't just get bigger, they also get **better**.
- Articles get better because Wikipedia is an **evolutionary system**.

> What makes evolutionary systems interesting?

<aside class="notes">
</aside>

The alternative to evolution
============================

![](img/god.jpg)

> And God said,  
> Let there be Wikipedia:  
> and there was Wikipedia.  

The alternative to evolution
============================

![](img/Jigsaw.svg.png)

<aside class="notes">
</aside>

===

![](img/logo.gif)

<aside class="notes">
</aside>

Key differences
===============
incremental: true

- design
- evaluation
- reducibility/independence

<aside class="notes">
design:
  If you want to make a puzzle the first thing you do is decide which picture to
  use. You start at the end, and work backwards from there. For Wikipedia
  articles, the idea is that the truth is already out there, the articles are
  already to some extent designed, and the job of a Wikipedia editor is to
  assemble that knowledge. For evolutionary systems, you don't need to know the
  end product. Darwin's theory allowed for design to emerge without a designer.
  Biological evolution is an algorithm for creating order from chaos.
evaluation:
  If you think about Wikipedia as a puzzle, then the only measure that really
  matters is how complete it is. How many articles are left? From an
  evolutionary perspective there is no notion of completeness. Things are never
  done evolving. The only way to evaluate an evolutionary system is to see if it
  is still improving. Is the current version of this thing in some way better
  than a previous version of the same thing?
reducibile:
  For puzzles, the unit of investigation is easy: if you want to know what makes
  a puzzle work, all you really have to do is study how individual pieces fit
  together, and the same thing applies to the whole puzzle. What this means is
  that multiple people can work on the same puzzle independently. This doesn't
  work the same way in evolution. You have to take a systems-level approach that
  understands how a change in one part of the system affects all the other
  parts. One of Darwin's key insights was to adopt a population-level thinking
  about species: that there wasn't really anything an individual could do to
  affect the species, it was the population that reflected the process of
  evolution. This same sentiment was echoed in Richard Dawkin's book "The
  Selfish Gene" which extends the idea of population-level thinking to the
  units of biological evolution, the genes.
</aside>

When will Wikipedia be finished?
================================

Wikipedia aims to be the "[sum of all human knowledge](https://en.wikipedia.org/wiki/User:Emijrp/All_human_knowledge)".

![](img/wikipedia-growth/growth-per-month.png)

![](img/wikipedia-growth/number-of-articles.png)

What's at stake?
================
incremental: true

Is understanding Wikipedia reducible to a single edit?

Is understanding conformity reducible to a single interaction?

Jacobs & Campbell (1961)
========================
type: prompt

The perpetuation of an arbitrary tradition through several generations of a
laboratory microculture.

Control groups
==============

![](img/jacobs_1961/fig1.png)

Not much faith in humanity!
===========================
incremental: true

> So labile is the autokinetic experience ... that one reading the reports of
  studies employing it might expect that an arbitrary group norm once
  established would be passed on indefinitely without diminution;

> that once well indoctrinated, the naive group members would become as rigid
  and reliable spokesmen for the norm as were the confederates who preceded
  them;

> that each new generation would unwittingly become a part of a
  self-perpetuating cultural conspiracy propagating superstition and falsehood.

(Actually, I think they're being sarcastic.)

Possible outcomes
=================

- depressing
- hero
- stubborn
- actual



The depressing outcome
======================

![plot of chunk unnamed-chunk-2](wikipedia-figure/unnamed-chunk-2-1.png) 

The hero outcome
================

![plot of chunk unnamed-chunk-3](wikipedia-figure/unnamed-chunk-3-1.png) 

The stubborn outcome
====================

Each person makes two generations of judgements.

![plot of chunk unnamed-chunk-4](wikipedia-figure/unnamed-chunk-4-1.png) 

The actual outcome
==================
type: prompt

Groups of two
=============

![](img/jacobs_1961/fig2.png)

Groups of three
===============

![](img/jacobs_1961/fig3.png)

![](img/jacobs_1961/fig5.png)

Groups of four
==============

![](img/jacobs_1961/fig4.png)

Conclusions
===========
incremental: true

> The inculcated arbitrary norms turn out to be eroded by innovation at a much
  more rapid rate than we had expected.

> We have hardly provided a laboratory paradigm for the examples of tenacious
  adherence to incredible superstition with which anthropologists and students of
  comparative religion provide us.

Discussion
==========
incremental: true

- What is the correct answer and how is it determined?
- Is conformity driving the effect, or is there a sense in which the illusion
  is induced by suggestion?
- Are there selection pressures driving responses to a particular value?
- What is the strongest predictor of "truth" in every group's mean judgements?

Flynn (2008)
============

Overimitation in children.

![](img/flynn_2008/puzzle-box.png)

![](img/flynn_2008/data.png)

Caldwell & Millen (2010)
========================

Iterative paper airplane building.

![](img/caldwell_2010/groups.png)

![](img/caldwell_2010/results.png)

Caldwell & Millen (2009)
========================

Imitation, emulation, and teaching in cumulative cultural evolution.

![](img/caldwell_2009/results.png)

<aside class="notes">
Actions: observe actual building of paper airplanes.
Results: inspect completed planes and observe flight distances.
Teaching: talk to other participants.
</aside>

Edits
=====

Article histories are linear.

![plot of chunk linear](wikipedia-figure/linear-1.png) 

But in reality there are a lot of reversions.

![plot of chunk revisions](wikipedia-figure/revisions-1.png) 

Edit trees
==========

A better way to show article histories is as a tree.

![plot of chunk tree](wikipedia-figure/tree-1.png) 

This is how edits should be counted.

![plot of chunk edits](wikipedia-figure/edits-1.png) 

Optimal edits
=============

![](img/optimal-edits/article-quality-1.png)

Optimal edits
=============

![](img/optimal-edits/activity-session.png)

![](img/optimal-edits/total-time-1.png)
