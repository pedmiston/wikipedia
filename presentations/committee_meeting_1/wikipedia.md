Wikipedia and cultural evolution
================================
author: Pierce Edmiston
date: 12/16/2015

Claims
======
- Wikipedia articles don't just get bigger, they also get **better**.
- Articles get better because Wikipedia is an **evolutionary system**.

<aside class="notes">
I'd like to make two claims about Wikipedia and cultural evolution. The first is
that Wikipedia articles don't just get bigger, they also get better.

This doesn't have to be true, of course. Articles could just get longer without 
parts being rewritten, and even if sections are being rewritten, maybe it's just
adding unneeded complexity or nuance, so they're changing but not reliably 
getting better.

So even though I don't think it's controversial to say that Wikipedia articles
get better, what's important is that it's an empirical question. Obviously a lot
hinges on what is defined as "better", but that's what makes it a psychological 
question. I think psychology in particular has a lot to say about what should 
count as better in the context of an online enyclopedia.

But before I get in to how to measure quality it's important to point out that 
even if it can be shown that Wikipedia articles do get better that doesn't 
necessarily explain why they get better, which brings me to my second claim: 
that articles get better because Wikipedia is an evolutionary system.
</aside>

What is an evolutionary system?
===============================
- An iterative process that yields better and better solutions to a problem 
  (e.g., genetic reproduction).
- Evolution requires **variation** and **selection**.
  - _Variation is what comes up with alternative solutions._
  - _Selection determines which solutions are better._

<aside class="notes">
An evolutionary system is an iterative process that yields better and better 
solutions to a problem. The iterative process that yields better and better
solutions to the problem of life is genetic reproduction. It's through
reproduction that mutations enter the gene pool.

This was Darwin's fundamental insight, that you could understand the variety of 
life using a simple algorithm: reproduce with some level of variation, and you 
are guaranteed to come up with better and better strategies for survival.
It's not about finding the best solution, it's just about finding a slightly
better solution.

In order to determine whether Wikipedia is an evolutionary system, we need
to determine whether there is variation and selection. We need a way
of coming up with alternative solutions, and a way of evaluating which
solutions are better.
</aside>

Variation in Wikipedia articles
===============================
[Around 10 edits are made every second](https://tools.wmflabs.org/wmcounter/).
- For biological species variation is achieved through reproduction.
- For cultural products variation is often by **blind design**.

<aside class="notes">
Since anyone with access to the internet can edit just about any page on 
Wikipedia, there is a huge amount of variability for any particular article. In 
fact, articles are edited on Wikipedia at a rate of 10 times per second.

Now presumably most of these edits are intended to be beneficial to the article,
which is one of the many ways in which cultural evolution is different from
genetic evolution.

For genetic evolution, variation is achieved through reproduction, and it is the
product of a random process. It's common to hear people say that evolution has
no foresight, it doesn't know what mutations are going to end up working. For
cultural products, however, variation is purposeful, but even if it's purposeful
that doesn't mean it always works. I'm going to call this type of variation
"variation by blind design".
</aside>

blind design
============
Changes made to a cultural product intended but not guaranteed to improve 
its chances of survival.

> The light bulb was an invention with 1,000 steps.
  -- _Thomas Edison_

<aside class="notes">
Variation by blind design is changes made to a cultural product intended but not
guaranteed to improve its chances of survival.

This apocryphal quote of Thomas Edison, that the light bulb was an invention 
with 1,000 steps, was in response to a reporter asking how it felt to fail 1,000
times. If Edison didn't learn anything from those 1,000 attempts--if later 
attempts were not influenced by previous attempts--they would have indeed been 
failures, but if the light bulb evolved, then the previous attempts were indeed 
mutations, even if in all cases the changes Edison was making were intended to 
improve the design.

My point is that you can think about source of variation on a spectrum, from the
random error that happens when bacteria replicates itself to the purposeful
process of generating and testing new ideas in human invention, but in all cases
evolution can still occur: it's the accumulation of better solutions that
matters for evolution, not where the solutions come from, so the fact that
Wikipedia edits are purposeful doesn't really matter for the overall
classification of Wikipedia as an evolutionary system.
</aside>

Not all edits are created equal
===============================
![](img/gorgeon_etal/web20_edits.png)
_(Gorgeon et al. 2011)_

<aside class="notes">
Even though we can include edits intended to improve an article
as true variation in an evolutionary sense, I think it's interesting
to note that not all edits are beneficial.

Here is a survey of a particular article, the article for "Web 2.0",
which had accumulated around 6,000 edits at the time this article
was written.

What's interesting to note is that there are a number of types of
edits: edits for promoting something somewhere else on the web,
test edits where the person is simply trying out the Wikipedia interface.

There is overt vandalism, which is usually easy to tell and those
edits can be reverted, there are maintenance edits to make the article
comply to certain style guides, but in sum it seems that at least
for this article a little bit less than half of the edits made to the
article were neutral: they probably don't have a noticeable impact 
on the quality of the article as a whole.

What's also surprising is that a relatively small percentage of the edits, 26%,
went unchallenged, that is, only about 1/4th of the edits made to this article
weren't subsequently rewritten.

I do think this is somewhat analogous to genetic evolution in that
most of the mutations made to genetic material are neutral, so
I think it's beneficial that there is such a diversity of types of
edits when thining about Wikipedia as an evolutionary system.
</aside>

Is Wikipedia an evolutionary system?
====================================
type: prompt
- Evolution requires variation and selection.
- Wikipedia articles vary, but is there selection?

<aside class="notes">
We've established that Wikipedia articles vary, and that it's ok
that these edits are made purposefully, but what remains is whether
or not there is selection for better and better articles.
</aside>

Selection
=========
``Survival of the fitter"

- Based on relative fitness (local optima).
- Contrasted with **random drift**.

![](img/pangloss.jpg)

<aside class="notes">
Natural selection is often described as survival of the fittest, but of course
fitness is not an absolute quality, so we might instead say that it is survival
of the fitter because all that matters is that certain variants are more or less
fit than others.

It is critically important to consider what the alternative to selection is,
otherwise we run the threat of sounding like Voltaire's character
Pangloss who thought that we were in the best of all possible worlds.

Here he's saying to Candide "So you see Candide..."

One of my favorite Panglossian arguments is that we have noses its the
best possible solution for our glasses to sit on.

The truth is that some things are the way they are for totally random
reasons; they weren't necessarily selected based on any objective measure of
fitness.

So in order to determine whether some system exhibits selection pressures
we need to compare it to the null hypothesis, that there is just random
drift of information.
</aside>

Random drift
============
The null hypothesis for selection.

![](img/bentley_etal/random_drift_model.png)

_(Bentley et al., 2004)_

<aside class="notes">
Random drift is the null hypothesis for selection when the process is still
iterative, so changes are accumulating, but they aren't accumulating for
any real reason. This process is actually very simple to model.

In this random drift model, winners in each generation are selected at random,
and what's interesting is that the end result is not a population with a uniform distributions of variants.
</aside>

Random drift
============
![plot of chunk unnamed-chunk-1](wikipedia-figure/unnamed-chunk-1-1.png) 
<aside class="notes">
In fact, what happens in a random drift model is that some of the variants
become very popular, but most are relatively rare.

One way to define selection processes at play is to demonstrate a deviation from
this neutral model: basically, you want to be able to say that particular 
variants survived for some non-random reason, and therefore the actual
frequencies would differ from the neutral model and that would be evidence
for selection.
</aside>

Random drift in cultural evolution
==================================
- first names
- pottery trends
- citations in patent applications

> We conclude that cultural and economic choices often reflect a decision process that is value-neutral. -- _Bentley et al., 2004_

<aside class="notes">
There are a lot of genetic products and a lot of cultural products whose 
frequency distributions are well predicted by the neutral model, and in these 
cases its hard to say why a certain variant is as popular as it is. 

For instance, in this paper, they show that a neutral model predicts the
frequency distribution of first names, pottery trends, and citations in patent 
applications. Their conclusion is that "cultural and economic choices often 
reflect a decision process that is value-neutral."

The takeaway from this is that selection isn't always obvious, but it can be
measured by comparing what actually happened to what might have happened if
winners were determined at random.
</aside>

Selection in Wikipedia articles
===============================
If articles are selected for quality, then more edited versions of an article
should be better than younger versions of the same article.
- Still haven't discussed measures of quality.
- But it's important that quality be measured "within-article".

<aside class="notes">
Now we can ask: is there selection in Wikipedia articles?

If the editing process is value-neutral, then even as articles accumulate more
and more edits, they don't necessarily get better and better. If, however, there
is selection pressure, than a version of an article with more edits should be
better than a previous version of that same article.

Note that I'm being very careful here to talk about relative improvements
in quality: selection is evidenced within a single article, rather than
edits being predictive of article quality overall.

So now we see why so much of this depends on how you measure article quality,
why I think this a question that needs to be answered by psychology.
If edits are reliably related to quality, then there is selection going
on among Wikipedia articles, and we can say that Wikipedia is an evolutionary
system. Further, if Wikipedia is an evolutionary system, then it means
that the most likely outcome of the continued editing of Wikipedia articles
is that they will just keep getting better.
</aside>

Castle (2004)
=============
![](img/castle-2004.png)

<aside class="notes">
Let's look at an example: here is the first entry for the article for
Castle, made in 2004. It describes a castle as a logical development
growing out of the need for fortification.
</aside>

Castle (2007)
=============
![](img/castle-2007.png)

<aside class="notes">
In 2007, the article focuses on the iconic nature of castles,
saying that castles are a symbol of the Middle Ages, and kind of
punting on what makes a castle other than it is distinct from
forts and fortresses.
</aside>

Castle (2015)
=============
![](img/castle-2015.png)

<aside class="notes">
In 2015, I think it's a pretty good description of castle.
It starts by describing a castle in terms of it being a
type of fortification built in a certain location and in
a certain time period, and goes on to distinguish castles
from palaces, fortresses, and settlements.

So now we can ask, what is changing about this article,
what is better in its current version than it was at the
beginning?
</aside>

What are the selection pressures?
=================================
[The perfect Wikipedia article](https://en.wikipedia.org/wiki/Wikipedia:The_perfect_article).

<aside class="notes">
In theory, what makes a good Wikipedia article can be objectively defined
and the community has attempted to describe just what makes a good
Wikipedia article.

Now this is not to say that all of the people who edit Wikipedia
do so in perfect concordance with these guidelines, although when
debates spring up over edits on the talk pages of each article,
people do tend to win arguments by deferring to specific principles.

One of the more interesting principles is that articles should be
written from a neutral point of view, and we see that in the castle
example: the article acknowledges that what makes a castle is debated,
but there is general agreement on the ways in which a castle differs
from a palace and a fortress.
</aside>

Measuring article quality
=========================
- Wikipedia's own ranking system.

![](img/article-quality/chart.png)

- Machine learning approaches?

<aside class="notes">
In an ideal world these guidelines would be easy to objectively measure and
whether or not Wikipedia articles improve would be easy to test.

Unfortunately, even with these guidelines, it's still hard to objectively
measure how articles become better and better over time. Presumably we should be
able to use Wikipedia's own ranking system for determining what makes a good
article, and one analysis I'd like to conduct is to see if articles
ascend the ranks of the Wikipedia rating system, as would be predicted 
if article quality is gradually improved.

But as far as I know the ranking system isn't validated, it's just vetted by
higher level editors, so we might want something a little more empirically
defensible.

I think it's possible and it's been attempted in the past, to train a classifier
to predict the quality of an article based on various metrics such as the number
of edits, number of unique editors, number of references, etc. but as with all
machine learning approaches, the conclusions are only as good as the training 
data, so really what we need is a psychological definition of article quality in
order to both validate Wikipedia's ranking system and to provide some training
data for more scalable approaches.
</aside>

A psychological definition of article quality
=============================================
_What can you do with a good article that you can't do with a bad article?_

Conventional
------------
- Learn more.

Unconventional
--------------
- Find an answer to a question more quickly.
- Explain it to someone else more accurately.

<aside class="notes">
I think that a psychological definition of article quality asks what
you can do with a good article that you can't do as well with a bad
article.

The conventional approach to this question would be to ask how much
you learn from a good article compared to how much you learn from
a bad article, but that approach gets messy for a number of reasons.
It's hard to control for the different motivations and prior knowledge
that people bring when given a learning problem, and more practically
speaking, it's hard to come up with good materials.

So instead I'd like to propose two unconventional approaches to 
measuring article quality, and we can see if there are others
I should be considering. The first unconventional approach is
to give someone a question and measure how long it takes them 
to find an answer. One way of measuring how two versions of an
article compare is to see if one version tends to lead to faster
answers than the other.

Now this has the problem of coming up with good materials too,
because we would want to ask the same question across different
versions of the article, and I would need to verify that the
correct information is present in each of the versions, but it
is doable.

A somewhat different approach is to think about what happens if
you were to read an article and pass on the information to another
person. In this experiment, people would read a section of an article
and pass it on to another person, like in the game of telephone,
and we would see for how many generations the message lasted
before it degraded below some threshold. Better quality articles
would last longer in this sort of iterative game than less quality
articles.

What's important about all of these measures is that they can be
evaluated for different versions of the same article, because
once a defensible definition of quality can be measured, then
we can answer those questions that I set out to answer at the
beginning.
</aside>

Claims, revisited
=================
- Wikipedia articles don't just get bigger, they also get **better**.
- Articles get better because Wikipedia is an **evolutionary system**.

Claims, revisited
=================
- Wikipedia articles don't just get bigger, they also get **better**.
- Articles get better because Wikipedia is an **evolutionary system**.

cf. Darwinian evolution
-----------------------
- Biological species don't just reproduce, they also get **better**.
- Biological species get better because genetic reproduction is an **evolutionary system**.

Implications
============
for the study of cultural evolution
-----------------------------------
- Extends cultural evolution to digital products.
- Opens the door for the study of adaptation, convergent evolution, and competition.

for the Wikimedia organization
------------------------------
- Optimization of editorial practices.

for psychology
--------------
- Evolutionary thinking provides a mechanism for the wisdom of the crowds.

Optimal edits
=============
![](img/article-quality-1.png)

Optimal edits
=============
![](img/activity-session.png)

![](img/total-time-1.png)

Baldwin Effect
==============
A mechanism for the wisdom of the crowd?

![plot of chunk unnamed-chunk-2](wikipedia-figure/unnamed-chunk-2-1.png) 

Goals
=====
incremental: true

Take advantage of Wikipedia's open data policies to investigate questions
about cultural evolution, in particular the notion that there are
consistently applied selection pressures for articles of better and better
quality.

- Experiment with various ways of operationalizing the quality
  of sections of Wikipedia articles with behavioral studies.
  - e.g., scavenger hunt, telephone game
- From these measures of quality, derive the characteristics of
  optimal edits in order to increase volunteer efficacy.
- Work backward to see how accurately article quality can be predicted
  using automated machine learning techniques.
- Begin to frame questions about article quality as an emergent property
  of Wikipedia.

Grant
=====

[Individual Engagement Grant](https://meta.wikimedia.org/wiki/Grants:IEG)
---

- Proposals accepted March 1-31.
- 6 month scope, with potential renewal.
- Maximum request is $30,000.

> Projects should foster conditions that encourage editing by volunteers.

Strategy
========
- Solicit feedback from [IdeaLab](https://meta.wikimedia.org/wiki/Grants:IdeaLab).
- Partnership with the OSF (**evergreen experiments**).

Project goals
=======
- To maximize editor efficiency by empirically deriving the kinds of edits
  that have the largest impact on the quality of an article.

Review
======
To study Wikipedia as an evolutionary system.
- Variations in edits
  - How are edits distributed throughout an article?
  - What's a better predictor of number of edits: article traffic or article size?

The growth of Wikipedia
=======================
- 10 [edits](https://tools.wmflabs.org/wmcounter/) per second.
- 700 [new articles](https://tools.wmflabs.org/wmcharts/wmchart0002.php) per day.
- English version has over 5 million articles.
- 7th most [trafficked](http://www.alexa.com/siteinfo/wikipedia.org) site in the world.
