Selection pressures in Wikipedia
================================
author: Pierce Edmiston
date: `r Sys.Date()`

```{r, config, echo = FALSE}
library(knitr)
opts_chunk$set(
  echo = FALSE
)
```

Claims
======

- Wikipedia articles don't just get bigger, they also get **better**.
- Articles get better because Wikipedia is an **evolutionary system**.

<aside class="notes">
</aside>

What is an evolutionary system?
===============================

- An iterative process that yields better and better solutions to a problem (e.g., genetic reproduction).
- Evolution requires **variation** and **selection**.

<aside class="notes">
</aside>

Why biological evolution?
=========================

Biology originated the idea of an ever-changing fitness landscape.

![](wikipedia-figure/fitness-landscape.png)

<aside class="notes">
</aside>

Article evolution
=================

![](wikipedia-figure/castle.png)

Measuring article quality
=========================

Wikipedia's own ranking system.

![](wikipedia-figure/article-quality-chart.png)

A psychological definition of article quality
=============================================

_What can you do with a good article that you can't do with a bad one?_

- Learn more (remember more facts, afford better inferences).
- Find an answer to a question more quickly.
- Repeat it to someone else more accurately.

Edits
=====

Article histories are linear.

```{r, linear, engine = "dot"}
digraph {
  t0 -> t1 -> t2;
  rankdir="LR";
}
```

But in reality there are a lot of reversions.

```{r, revisions, engine = "dot"}
digraph {
  t0 -> t1 -> t2 -> t3;
  t0[label="v0", color="blue"];
  t1[label="v1", color="green"];
  t2[label="v2", color="red"];
  t3[label="v1", color="green"];
  rankdir="LR";
}
```

Edit trees
==========

A better way to show article histories is as a tree.

```{r, tree, engine = "dot"}
digraph {
  v0 -> v1 -> v0 -> v2 -> v3 -> v4 -> v3 -> v5 -> v3 -> v6 -> v7;

  v0[color="blue"];
  v2[color="green"];
  v3[color="red"];
  v6[color="orange"];
  v7[color="purple"];

  rankdir="LR";
}
```

This is how edits should be counted.

```{r, edits, engine = "dot"}
graph {
  v0 -- {v1, v2};
  v2 -- v3 -- {v4, v5, v6};
  v6 -- v7;

  v0[label="0", color="blue"];
  v2[label="1", color="green"];
  v3[label="2", color="red"];
  v6[label="3", color="orange"];
  v7[label="?", color="purple"];
  
  // dead ends
  v1[label=""];
  v4[label=""];
  v5[label=""];

  rankdir="LR";
}
```

Optimal edits
=============

![](wikipedia-figure/article-quality-1.png)

Optimal edits
=============

![](wikipedia-figure/activity-session.png)

![](wikipedia-figure/total-time-1.png)

First steps
===========

- Learn the mathematics for understanding fitness landscapes.
- Write some scripts to parse Wikipedia article histories.
- Investigate relationships between number of edits and article quality,
  including number of unique editors, size of article, page views, etc.
- Run some simple experiments in which different versions of the same articles
  are compared (subjective judgments of quality, scavenger hunt, telephone game).