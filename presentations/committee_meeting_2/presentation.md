Prelim defense
==============
css: theme.css

Pierce Edmiston  
May 23, 2016

Recap
=====
incremental: true
**Write a grant**  
to fund the study of the evolution of Wikipedia articles.

**Write a review paper**  
to defend why evolution is a good model for Wikipedia.

Wikipedia as an evolutionary system
===================================
type: centered
<div class="image-row">
  <img src="img/evolutionary-trees.png" />
  <img src="img/neutral-model.png" />
  <img src="img/fitness-landscape.png" />
</div>

<aside class="notes">
</aside>

Evolution is not a tree
=======================
![](img/evolutionary-trees.png)

- Evolution requires variation, **selection**, and inheritance.
- Evolution can look like a tree but it doesn't have to.
- Drawing a tree is not proof of evolution.

<aside class="notes">
I wanted to start by defining evolution, but the problem is that the 
definition of evolution all by itself isn't very useful, at least
not to me. The easy definition of evolution is that it is anything
that has variation, selection, and inheritance.

Variation: different versions of essentially the same thing.
Selection: a way to compare different versions and measure which one is better.
Inheritance: are the winners carried on to the next round?

My problem with this definition is that its very vague and makes it seem
like evolution is a pretty low bar. For a dumb example, every year my vegetable
garden varies, and I make an effort to repeat things that worked the previous
year. Is it really a meaingful definition of evolution if all it takes
is the logical argument that a system meets these three criteria? What does
calling something an evolutionary system actually buy us?

So the way to get by this vagueness is to contrast evolution with something,
and I decided to contrast evolution with a tree, because I think it
captures a lot of the difficulties people have when applying evolutionary
principles to cultural phenomena.

What's interesting about this contrast is that a lot of people think that
evolution **is** a tree. Well, evolution can look like a tree. There is
a whole field of phylogenetics that emerged from Darwinian theory based
on the drawing of evolutionary trees. But it doesn't have to look like
a tree.

The image on the right is not a tree, or at least it's not any tree I've
every seen. This image came from a paper arguing against cultural evolution
on the idea that biological evolution could always be represented as a tree
but cultural evolution never could because inheritance was messy, and
the different lines were always blending and mixing.

The truth is that biological inheritance can be messy too; there is blending
and mixing in our evolutionary past; in which case there really isn't a
separation between biological and cultural evolution in terms of tree-like-ness.

The more interesting conclusion to draw from the contrast of evolution
to trees is that drawing a tree is not proof of evolution. Even if
the assumptions that allow phylogenetists to draw evolutionary trees can
be met, that doesn't actually prove that evolution is happening.

In particular, it doesn't prove that selection is happening. People can
and often do run phylogenetic analyses on non-functional parts of DNA
that are by definition not under selection pressures.
</aside>

Measuring non-randomness
========================
![](img/neutral-model.png)

- Can't assume selection is occuring.
- Frequency distributions can be random.
- _Need prediction!_

<aside class="notes">
</aside>

Wikipedia's fitness landscape
=============================
![](img/fitness-landscape.png)

- Edits vary, accumulate, and are selected.
- Can changes in article quality be predicted?
- **Process, not product.**

<aside class="notes">
</aside>

Monotonic growth
================
type: centered
![](img/elena_lenski/fitness.png)

1000 random articles
====================
type: centered










<img src="fig/qualities-1.png" title="plot of chunk qualities" alt="plot of chunk qualities" style="display: block; margin: auto;" />

Learning from Wikipedia article histories
=========================================
type: centered
<img src="fig/wikipedia-article-histories-1.png" title="plot of chunk wikipedia-article-histories" alt="plot of chunk wikipedia-article-histories" style="display: block; margin: auto;" />

If Wikipedia is an evolutionary system
======================================
incremental: true
- We can probably trust Wikipedia more than we do.
- We can extend this sort of analysis to new domains.
- We can start to come up with ways to improve it.

What is article quality?
========================
type: bigquote

> The benefit to modeling Wikipedia as an evolution system is that it inverts the definition of article quality to being about what content survives in an article as it is continually edited, rather than trying to define a priori the constituents of article quality that apply for all articles.

Objective Revision Evaluation Service
=====================================
The [ORES](https://meta.wikimedia.org/wiki/ORES) provides machine learning models as a service.
- article quality models: [wp10](https://meta.wikimedia.org/wiki/ORES/wp10)
- edit quality models: [damaging](https://meta.wikimedia.org/wiki/ORES/damaging), [goodfaith](https://meta.wikimedia.org/wiki/ORES/goodfaith), [reverted](https://meta.wikimedia.org/wiki/ORES/reverted)

Open Source Software
====================
**Brooks' Law**  
Adding manpower to a late software project makes it later.

**Linus's Law**  
Given enough eyeballs, all bugs are shallow.
