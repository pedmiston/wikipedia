Wikipedia as a model for cumulative cultural evolution
======================================================
author: Pierce Edmiston
date: 2015-11-04

Universal Darwinism
===================

![](images/darwin-caricature.jpg)

- variation
- selection
- retention

Cultural Evolution
==================

> Culture is information capable of affecting individuals' behavior that they aquire from other members of their species through teaching, imitation, and other forms of social transmission. (Boyd & Richerson)

- variation
- selection
- retention

What does it mean to be a system that evolves?
==============================================

- Evolution is not a synonym for change, it is the science of change.
- There are laws of evolution that apply to all members across all levels of analysis (micro to macro).
- It wasn't until 1930s and 1940s (~70 years after Darwin) that evolutionary biology really took off.

> Nothing in biology makes sense except in the light of evolution. (Theodosius Dobzhansky in **1973**)

Modern synthesis in biological sciences
=======================================

- Consensus on the mechanisms of evolution was reached in the 1930s and 1940s.
- Microevolutionary changes give rise to macroevolutionary effects.
- Mechanisms include: **natural selection**, **particulate inheritance**, **branching lineages**.

Seeking a modern synthesis in social sciences
=============================================

This is what Mesoudi et al. is after. But there are problems with applying the tenants of the modern synthesis to cultural evolution.

- natural selection versus random drift
- particulate inheritance versus blended inheritance
- branching lineage versus blended lineage

Natural selection versus random drift
=====================================

![](images/bentley_etal/random_drift_model.png)

Random drift
============

![plot of chunk unnamed-chunk-1](wikipedia-figure/unnamed-chunk-1-1.png) 

First names drift randomly
==========================

![](images/bentley_etal/first_names.png)

So do patent citations
======================

![](images/bentley_etal/patents.png)

Natural selection versus random drift
=====================================
type: prompt

If cultural evolution is to be taken seriously, we need to consider cases where there are strong selection pressures (deviations from a random drift model).

Particulate inheritance versus blended inheritance
==================================================

- Cultural evolution is the evolution of information "in the head".
- Memes were proposed as a logical analog to genes, but the analogy is problematic.
- In fact, even the idea that genetic evolution is digital (Mendelian genetics) might not the whole story of genetic evolution.

Particulate inheritance versus blended inheritance
==================================================
type: prompt

It's unclear whether cultural evolution operates on memes, but it's also unclear whether genetic evolution needs Mendelian inheritance (e.g., asexual organisms).

Branching lineage versus blended lineage
========================================

> Biological evolution is a system of constant divergence without subsequent joining of branches. Lineages, once distinct, are separate forever. In human history, transmission across lineages is, perhaps, the major source of cultural change. (Gould, 1991)

![](images/on-the-origin-tree.jpg)

Branching lineage versus blended lineage
========================================
type: prompt

No perfectly bifurcating tree of life.

- polytomy happens
- viruses move DNA across lineages
- e.g., the bacteria tree of life might actually be a ring

What can evolutionary social science learn from evolutionary biology?
=====================================================================

Archaeology and phylogenies
===========================

![](images/obrien_etal_2001/variation.png)

Archaeology and phylogenies
===========================

![](images/obrien_etal_2001/quantified.png)

Archaeology and phylogenies
===========================

![](images/obrien_etal_2001/phylogeny.png)

The distribution of the variants matters
========================================

![](images/mesoudi_obrien/bettinger_erkens_variability.png)

A cultural evolution perspective
================================

![](images/mesoudi_obrien/fitness_landscape.png)

Hunting success
===============

![](images/mesoudi_obrien/results.png)

Correlations?
=============

![](images/mesoudi_obrien/variance.png)

Conclusions from Mesoudi & O'Brien
==================================

- When you see a better option, you take it.
- When you don't, you try (randomly) to get better.
- But not everything in cultural evolution has such a simple fitness landscape.

Great for understanding history!
================================
type: prompt

Adopting an evolutionary framework gives social scientists tools for recreating historical change. But what about cognitive scientists?

Cognitive scientists should adopt an evolutionary framework for explaining group behaviors that are different than individual behaviors.

Emergent processes in group behavior
====================================

![](images/goldstone_roberts_2008.png)

A proposal
==========

- Evolutionary biology has bacteria, fruit flies, pea plants, etc.
- These model organisms are chosen because they reproduce quickly, they have small genomes, they can survive in the lab, etc.
- What are the model organisms for cultural evolution?

Requirements for selecting a model organism
===========================================

1. appropriateness as an analog
2. transferability of information
3. genetic uniformity of organisms, where applicable
4. background knowledge of biological properties
5. cost and availability
6. generalizability of the results
7. ease of and adaptability to experimental manipulation
8. ecological consequences
9. ethical implications

Wikipedia is alive
==================

- [edits](https://tools.wmflabs.org/wmcounter/)
- [new articles](https://tools.wmflabs.org/wmcharts/wmchart0002.php)
- [deletions and restorations](http://tools.wmflabs.org/wmcharts/wmchart0004.php)
- [traffic](http://www.alexa.com/siteinfo/wikipedia.org)

The free encyclopedia that anyone can edit
==========================================

- Most attention to Wikipedia has concerned its accuracy.
- What's more interesting is whether it improves.
- **Quality is a selection pressure.**

So, is Wikipedia any good?

===

![](images/epistemic-balance.jpg)

Wikipedias in different languages
=================================

- 291 different Wikipedias (different languages).
- Rewinding the evolutionary clock?

English and Polish Wikipedias
=============================

![](images/callahan_etal/english_polish_wikipedia.png)

English and Polish Wikipedias
=============================

![](images/callahan_etal/case_studies.png)

Analogies to experimental evolution
===================================

![](images/elena_lenski/compromise.png)

The life cycle of a Wikipedia article
=====================================

Free will
=========

![](images/mattus/free_will.png)

Fell (it's some Swedish thing)
==============================

![](images/mattus/fell.png)

Edgar Allan Poe
===============

![](images/mattus/poe.png)

Analogies to experimental evolution
===================================

![](images/elena_lenski/fitness.png)

From an evolutionary perspective, articles with more edits should be of higher quality.

What is an edit?
================

- a mutation to the article
- most are probably neutral
- words that aren't edited survive

Web 2.0 edits
=============

![](images/gorgeon_etal/web20_edits.png)

Distribution of edits
=====================

![](images/wilkinson_huberman_2007/log_normal_edits.jpg)

Analogies to experimental evolution
===================================

![](images/elena_lenski/mutations.png)

Edits per article
=================

![](images/wilkinson_huberman_2007/articles_by_edits.jpg)

Timeline of a Wikipedia article
===============================

![](images/wilkinson_huberman_2007/edits_by_article_age.jpg)

Relationship between edits and quality
======================================

![](images/wilkinson_huberman_2007/quality.jpg)

Article quality
===============

![](images/wikipedia_statistics/article-quality.png)

Darwin would have loved Wikipedia
=================================
type: prompt

There has yet to be a systematic study of the transformation of Wikipedia articles over time with a direct comparison to the mechanisms of evolution.

Articles **vary** as a function of edits, they are **selected** as a function of quality, and, since it's digital, all changes are **inherited**.

If the goal of cultural evolution is to provide a synthesis for all social sciences, then any relationships governing the evolution of Wikipedia articles should also apply to any culturally evolved systems.

Data!
=====

- [every edit, ever](https://dumps.wikimedia.org/backup-index.html)
- [inbound traffic](http://www.online-utility.org/wikipedia/top_500_websites_wikipedia.jsp)
- [wikiscanner](http://virgil.gr/wikiscanner/)
