I want to start by very briefly recapping the three documents I've submitted for review.

First was the grant proposal I submitted to the Wikimedia Foundation. The goal here was to outline the sorts of questions that could be answered when articles are viewed as evolutionary trees of edits, and all of the questions basically had to do with assessing the efficiency of the Wikipedia editing strategy. It all started with assessing the likelihood that good quality edits survived and bad edits were removed. But once that was figured out, there were all sorts of other questions that you could ask about successful editing strategies. For example, was there an ideal number of editors that lead to the most efficient use of editor time? What about edit size, can we show why edits that are too big are inefficient for Wikipedia using a comparison of edit size to mutation rate in biological systems?

This one was tough because I had to strike a balance between making the efficiency of Wikipedia a scientific question while at the same time appealing to Wikipedia editors who tend to believe that all of their edits are good, and they didn't really think this sort of analysis would help them.

This is part of the motivation for the second paper I submitted, which tried to outline in detail what it meant to call Wikipedia an evolutionary system. In the paper I tried to highlight the commonalities and misconceptions when it comes to comparing biological and cultural evolution, and to describe how the design of Wikipedia makes certain types of outcomes very likely--namely that articles will tend to improve monotonically in terms of article quality.

Now I got a lot of feedback from the committee, most of it dealing with the fact that calling Wikipedia an evolutionary system doesn't really mean much at all, it's about the properties of the system and how evolutionary theory informs how we evaluate and understand the history of Wikipedia articles.

Gary: "Wikipedia is an evolutionary system" is not the right approach.
Morton: If I'm going to claim that Wikipedia articles improve I better be citing the literature on Wikipedia article quality.
Simon: I've got to be careful using broad terms like evolutionary systems when not all of evolution is cumulative, Darwinian evolution.
Mark: Is article quality as defined by the Wikipedia community really the best measure of article quality?

To be honest, this feedback made this paper almost unsavable, or at least it wasn't likely to be published any time soon, so what I did was to basically scrap it and start over: this time focusing on a specific problem that I though evolutionary theory could help me answer.

And the problem was: How successful is the Wikipedia editing strategy? How likely is it that high quality content survives and low quality content is removed? My argument is that this measure is the best measure of the success of Wikipedia, and an important factor in how we think about the reliability of Wikipedia articles.

I've spent a lot of time talking about Wikipedia and the evolution of articles, but I had all these doubts about whether or not I was on to something, and I think it was really important to verify a lot of the assumptions I had going in to this process.

The first assumption was that article revision histories really do reflect this sort of iterative, edit-revise-resubmit cycle, and so that's why I started with a data visualization problem: how to turn a linear sequence of edits into an evolutionary tree of edits and how do actually Wikipedia articles look?